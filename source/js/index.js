import '../css/style.scss'
//-import 'slick-carousel'
import ViewPort from './modules/module.viewport'
import Tab from './modules/module.tab'
import './modules/module.form'
import app from './modules/module.app'
import Popup from './modules/module.popup'

// require('malihu-custom-scrollbar-plugin')($);

window.app = {};

$(()=>{

	let jsCalcResult = $('.js-calc-result').eq(0).html();

	console.info(app);

	let $b = $('body');

	window.app.popup = new Popup({
		bodyClass: 'popup-is-open',
		onPopupOpen: (pop)=>{
			app.fullpage.hold(true);

			if ( $(pop).hasClass('popup_order') ){

				let jsCalcResultNew = $('.js-calc-result').eq(0).html();

				if (jsCalcResult !== jsCalcResultNew) {
					jsCalcResult = jsCalcResultNew;
					$(`[data-order-calories="${jsCalcResult}"]`).prop('checked', true);
				}

				$(pop).find('.order__step_1').find('input').eq(0).trigger('change');

			}
		},
		onPopupClose: (pop)=>{
			app.fullpage.hold(false);
			$b.removeClass('is-menu-show');
		},
	});

	new Tab({
		onTabChange:(ind, group)=>{
			console.info(ind, group);

			if (group === 'menu'){

				let all = $(`[data-tab-group="${group}"]`).find('input')
					// current = $(`[data-tab-target="${ind}"]`).find('input').eq(0)
				;
				all.prop('checked', false);
				// current.prop('checked', true).trigger('change');

				$(`[data-tab-target="${ind}"]`).each(function () {
					$(this).find('input').eq(0).prop('checked', true).trigger('change');
				})
			}
		}
	});

	$b
		.on('change', '[data-menu-choose]', (e)=>{
			let $imageWrap = $(e.target).closest('.section'),
				$imageCurrent = $(`[data-menu-target="${e.target.getAttribute('data-menu-choose')}"]`)
			;
			$imageCurrent.addClass('is-active').siblings('.is-active').removeClass('is-active');

			// console.info($imageWrap, $imageAll, $imageCurrent);

		})
		.on('change', '[data-menu-switch]', function(){
			let $form = $(this).closest('form'),
				$image = $('[data-menu-image]'),
				$img = $image.find('img'),
				$content = $('[data-menu-content]'),
				$comment = $('[data-menu-comment]'),
				$price = $('[data-menu-price]')
			;

			$b.addClass('is-loading');

			$.ajax({
				url: $form.attr('action'),
				method: $form.attr('method'),
				data: $form.serialize(),
				dataType: 'json',
			})
				.always((e)=>{
					$b.removeClass('is-loading');
				})
				.done((e)=>{
					console.info('done', e);

					$comment.html(e.comment);
					$price.html(e.price);
					$content.html(e.content);

					if ( e.calories ){
						$(`[data-order-calories="${e.calories}"]`).prop('checked', true);
					}

					if ( $img.attr('src') !== e.image ){
						$image.removeClass('is-active');
						setTimeout(()=>{
							$image.addClass('is-active');
							$img.attr('src', '');
							$img.attr('src', e.image);
						}, 500);
					}
				})
				.fail((e)=>{
					console.info('fail', e);
				})
			;

		})
		.on('change', '.order__step_1 input', function(){
			let $input = $(this),
				$form = $input.closest('[data-form]'),
				$comment = $form.find('.order__foot-info'),
				$price = $form.find('.order__foot-price')
			;

			$b.addClass('is-loading');

			$.ajax({
				url: $form.attr('data-action'),
				method: $form.attr('data-method'),
				data: $form.find('input').serialize(),
				dataType: 'json',
			})
				.always((e)=>{
					$b.removeClass('is-loading');
				})
				.done((e)=>{
					console.info('done', e);

					$comment.html(e.comment);
					$price.html(e.price);

				})
				.fail((e)=>{
					console.info('fail', e);
				})
			;

		})
	;

	app.fullpage.init();
	app.calculator.init();
	app.range.init();
	app.order.init();
	app.dialogShaker.init();

	new ViewPort({
		'0': ()=>{
			window.breakpoint = 'mobile';
			// app.fullpage.destroy();
			app.fullpage.setView(window.breakpoint);

		},
		'1024': ()=>{
			window.breakpoint = 'desktop';
			// app.fullpage.init();
			app.fullpage.setView(window.breakpoint);
		}
	});


	$b
		.on('click', '[data-scroll]', function(e){
			e.preventDefault();
			$('html,body').animate({scrollTop: $($(this).attr('data-scroll')).offset().top}, 500);
		})

		.on('click', '.header__handler', (e)=>{
			$b.toggleClass('is-menu-show');
			app.fullpage.hold($b.hasClass('is-menu-show'));
		})

		.on('click', '.nav__menu-link', (e)=>{
			$b.removeClass('is-menu-show');
			app.fullpage.hold(false);
		})


		.on('click', '.dialog__ico', function(e){
			$(this).closest('.dialog').toggleClass('dialog_open');
		})
		.on('click', '.dialog__content-close', function(e){
			$(this).closest('.dialog').removeClass('dialog_open');
		})

	;


});