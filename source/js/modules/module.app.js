import anime from 'animejs/lib/anime.es'
import noUiSlider from 'nouislider'
import Form from './module.validate'

import 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min'
const form = new Form();

import Fullpage from 'fullpage.js';

let app = {

	fullpage: {

		inited: false,


		hold: (e)=>{
			try {
				window.fullpage_api.setAllowScrolling(!e);
				window.fullpage_api.setKeyboardScrolling(!e);
			} catch (e){}
		},

		init: ()=>{
			new Fullpage('.website', {
				verticalCentered: false,
				normalScrollElements: '.popup',
				// scrollOverflow: true,
				menu: '.js-menu',
				// autoScrolling: false,
				// fitToSection: false,
				anchors: ['welcome', 'menu', 'advantage', 'delivery', 'social', 'review', 'faq', 'footer'],
				onLeave:(prevIndex, nextIndex, direction)=>{
					console.info(nextIndex);
					app.plate.animate(nextIndex.anchor);

					$('[data-menuanchor]').removeClass('active');
					$(`[data-menuanchor="${nextIndex.anchor}"]`).addClass('active');

					let section = $(nextIndex.item).attr('data-section') || '',
						index = (nextIndex.index + 1).toString().length < 2 ? ('0' + (nextIndex.index + 1)) : nextIndex.index + 1,
						indexNext = (nextIndex.index + 2).toString().length < 2 ? ('0' + (nextIndex.index + 2)) : nextIndex.index + 2
					;

					$('.navigation__section').html(section);
					$('.navigation__index').html(index);
					$('.navigation__next').html(indexNext);
				},
				afterLoad:(origin, destination, direction)=>{

				},
				afterRender:()=>{
					$('.section__aside_scroll').mCustomScrollbar();
				}
			});


			if ( !app.fullpage.inited ){

				app.fullpage.inited = true;

				$('body')

					.on('click', '.navigation__arrow', function(e){
						let down = $(this).hasClass('navigation__arrow_down');

						if ( down ){
							try { window.fullpage_api.moveSectionDown(); } catch (e){}
						} else {
							try { window.fullpage_api.moveSectionUp(); } catch (e){}
						}

					})

				;
			}

		},

		setView: (view)=>{
			switch (view){
				case 'desktop':

					$('.section__aside_scroll').mCustomScrollbar();
					$('.popup__content').mCustomScrollbar();
					try {
						window.fullpage_api.setAutoScrolling(true);
						window.fullpage_api.setFitToSection(true);
						// window.fullpage_api.setResponsive(true);
					} catch (e){}
					break;

				case 'mobile':

					$('.section__aside_scroll').mCustomScrollbar('destroy');
					$('.popup__content').mCustomScrollbar('destroy');
					try {
						window.fullpage_api.setAutoScrolling(false);
						window.fullpage_api.setFitToSection(false);
						// window.fullpage_api.setResponsive(false);
					} catch (e){}
					break;
			}
		},

		destroy: ()=>{
			$('.section__aside_scroll').mCustomScrollbar('destroy');
			try { window.fullpage_api.destroy('all'); } catch (e){}
		},


	},

	plate: {

		$path: $('.plate').find('path'),

		state: {
			welcome: 'M100 0 L100 100 L60 100 L40 0 Z',
			menu: 'M100 0 L100 100 L42 100 L42 0 Z',
			advantage: 'M100 0 L100 100 42 100 L58 0 Z',
			delivery: 'M100 0 L100 100 L58 100 L42 0 Z',
			social: 'M100 0 L100 100 L42 100 L42 0 Z',
			review: 'M100 0 L100 100 42 100 L58 0 Z',
			faq: 'M100 0 L100 100 L58 100 L42 0 Z',
			footer: 'M100 0 L100 100 L100 100 L100 0 Z',
		},


		animate: (position)=>{

			if ( !app.plate.state[position] ){
				console.warn('that state is none');
				return false;
			}

			anime({
				targets: app.plate.$path[0],
				loop: false,
				d: [
					{
						easing: 'easeOutBack',
						duration: 800,
						value: app.plate.state[position]
					}
				],
				complete: ()=>{

				}
			})
		},
	},


	calculator: {

		getResult:()=>{

			let data = {},
				$form = $('.calculator'),
				arr = $form.serializeArray(),
				result = true
			;

			for (let i = 0; i < arr.length; i++){
				data[arr[i].name] = arr[i].value;
				if ( !arr[i].value ) result = !1;
			}

			if ( result ){

				result = ( (data.gender === "male" ? (+5) : (-161)) + 9.99 * data.weight + 6.25 * data.height - 4.92 * data.age) * parseFloat( data.activity ) ;
				result = Math.round(result + result * parseFloat(data.effect));

			} else {
				result = '----'
			}

			$form.find('.js-calc-result').html(result);

			if (window.jigApp && window.jigApp.calcResult && typeof window.jigApp.calcResult === 'function') window.jigApp.calcResult();

		},

		init: ()=>{

			$('.calculator').find('input').on('change', function(e){

				app.calculator.getResult();

				let $t = $(this),
					max = $t.attr('max'),
					min = $t.attr('min')
				;

				if ( e.target.type === 'text'){
					if ( +$t.val() < min ){
						$t.val(min);
					} else if (  +$t.val() > max ){
						$t.val(max);
					}
				}

			});


		}
	},


	range: {
		secToTime:(val)=>{
			let hours = parseInt(val / 60),
				minutes = Math.floor(val % 60),
				days = parseInt(minutes / 60)
			;

			hours = hours % 60;

			if(String(hours).length !== 2) hours = "0" + hours;
			if(String(minutes).length !== 2) minutes = "0" + minutes;
			if( days === 0 ) { return hours + ":" + minutes; }
			return days + ":" + hours + ":" + minutes;
		},

		init: ()=>{
			$('.form__range').each(function () {
				let $range = $(this),
					$line = $range.find('.form__range-line'),
					values = [],
					min = +$range.attr('data-min'),
					max = +$range.attr('data-max'),
					step = +$range.attr('data-step'),
					margin = +$range.attr('data-margin'),
					pmin = $range.attr('data-from') ? +$range.attr('data-from') : min ,
					pmax = $range.attr('data-to') ? +$range.attr('data-to') : max,

					$min = $range.find('.form__range-edge_min'),
					$max = $range.find('.form__range-edge_max'),
					$inputFrom = $range.find('.form__range-input_from'),
					$inputTo = $range.find('.form__range-input_to')
				;

				$min.html(app.range.secToTime(min));
				$max.html(app.range.secToTime(max));

				if ($range.hasClass('is-inited')) return false;
				$range.addClass('is-inited');

				if (min === 'NaN' || max === 'NaN' || min === 'undefined' || max === 'undefined' ){
					console.error('Добавьте минимальное и максимаьлное значение фильтра');
					return false;
				}

				let slider = noUiSlider.create($line[0], {
					start: [pmin, pmax],
					connect: true,
					step: step,
					direction: 'rtl',
					margin: margin,
					range: {
						'min': min,
						'max': max
					}
				});


				$line[0].noUiSlider.on('end', ()=>{

				});

				$line[0].noUiSlider.on('update', (val)=>{

					$inputFrom.val(+val[0]);
					$inputTo.val(+val[1]);

					$line.find('.noUi-handle-lower').html(`<span>${app.range.secToTime(+val[0])}</span>`);
					$line.find('.noUi-handle-upper').html(`<span>${app.range.secToTime(+val[1])}</span>`);

				});


				$line[0].noUiSlider.on('set', (val)=>{

				});


			});
		}
	},

	dialogShaker: {

		pushed: false,

		interval: null,

		shake: ()=>{
			if ( !!window.localStorage.getItem('dialogShaker') ) {
				clearInterval(start);
			} else if (!app.dialogShaker.pushed) {
				anime({
					targets: '.dialog__ico',
					translateY: -30,
					loop: 1,
					direction: 'alternate',
					easing: 'easeInOutSine',

					duration: 300,
					complete: ()=>{

					}
				})
			}
		},

		init:()=>{
			// if ( !window.localStorage.getItem('dialogShaker') ){
				app.dialogShaker.interval = setInterval(()=>{
					app.dialogShaker.shake();
				}, 5000);
			// }


			$('body').on('click', '.dialog__ico', ()=>{
				app.dialogShaker.pushed = !0;
				//window.localStorage.setItem('dialogShaker', true);
			})

		}
	},

	order:{

		init: ()=>{

			$('body')
				.on('click', '[data-setvalue]', function () {
					let $t = $(this),
						$field = $t.closest('.form__field'),
						value = $t.attr('data-setvalue')
					;

					$field.find('input').val(value);

				})
				.on('click', '[data-order-step]', function () {
					let $t = $(this),
						$order = $t.closest('.popup'),
						step = $t.attr('data-order-step'),
						$currentForm = $order.find('.order__step.is-active'),
						prev = $order.find('.order__nav-item.is-active').attr('data-order-step') || 0
					;


					let $item = $currentForm.find('input, textarea, select'),
						wrong = false
					;

					$item.closest('.form__field').removeClass('f-error');


					if ( step > prev ){

						$item.each(function () {
							let input = $(this), rule = $(this).attr('data-require');

							if ( rule ){

								form.validate(input[0], rule, err =>{
									if (err.errors.length) {
										wrong = true;
										let data = [{
											name: input[0].name,
											error: true,
											message: err.errors[0]
										}];

										input.closest('.form__field').addClass('f-error');

										console.info(data);
									}
								});

							}

						});
					}

					if ( !wrong ){
						$('.order__step, .order__nav-item').removeClass('is-active');
						$(`.order__step_${step}, .order__nav-item[data-order-step="${step}"]`).addClass('is-active');
					} else {

						$('.order__form-inner').animate({scrollTop: 0}, 500);
					}

				})
		}
	}

};


export default app;